import pymysql
import config

#connect to localhost : change password !
db = pymysql.connect(host=config.DBHOST, user=config.DBUSER, password=config.DBPASS, db=config.DBNAME)
cursor = db.cursor()
cursor.execute("SHOW DATABASES LIKE '"+config.DBNAME+"'")
collection = cursor.fetchall()
if collection == ():
    print("to init")
else:
    db = pymysql.connect(host=config.DBHOST, user=config.DBUSER, password=config.DBPASS, db=config.DBNAME)
    cursor = db.cursor()


def setconnection():
    db = pymysql.connect(host=config.DBHOST, user=config.DBUSER, password=config.DBPASS, db=config.DBNAME)
    return db,db.cursor()

def setupDB():
    db = pymysql.connect(host=config.DBHOST, user=config.DBUSER, password=config.DBPASS, db=config.DBNAME)
    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    cursor.execute("""
    CREATE TABLE IF NOT EXISTS Workshop (
        code VARCHAR(40) BINARY PRIMARY KEY,
        titre VARCHAR(255) NOT NULL,
        derive BOOLEAN
    )""")


    cursor.execute("""
    CREATE TABLE IF NOT EXISTS Item (
        code VARCHAR(40) BINARY PRIMARY KEY,
        path VARCHAR(1023) BINARY,
        workshop VARCHAR(40) BINARY,
        FOREIGN KEY (workshop) REFERENCES Workshop(code)
    )""")

    cursor.execute("""
    CREATE TABLE IF NOT EXISTS Author (
        name VARCHAR(255) BINARY NOT NULL,
        PRIMARY KEY(name)
    )""")

    cursor.execute("""
    CREATE TABLE IF NOT EXISTS History (
        id INTEGER AUTO_INCREMENT,
        date DATETIME NOT NULL,
        author VARCHAR(255) BINARY NOT NULL,
        item VARCHAR(40) BINARY,
        FOREIGN KEY (author) REFERENCES Author(name),
        FOREIGN KEY (item) REFERENCES Item(code),
        PRIMARY KEY(id)
    )""")

    cursor.execute("""
    CREATE TABLE IF NOT EXISTS UpdateTime(
        date_update INT PRIMARY KEY
    )
    """)



   
    db.close()


def showTables():
    sql = "SHOW TABLES"
    cursor.execute(sql)
    cursor.fetchall() 

def resetDB():

    cursor = db.cursor()
    sql = "DROP TABLE IF EXISTS {}".format('History')
    cursor.execute(sql)
    sql = "DROP TABLE IF EXISTS {}".format('Author')
    cursor.execute(sql)
 
    sql = "DROP TABLE IF EXISTS {}".format('Item')
    cursor.execute(sql)
    sql = "DROP TABLE IF EXISTS {}".format('Workshop')
    cursor.execute(sql)
    sql = "DROP TABLE IF EXISTS {}".format('UpdateTime')
    cursor.execute(sql)
    db.close()





#setupDB()

#cursor.execute("DROP DATABASE activity_authors")


