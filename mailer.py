import os
import glob
import imghdr
import smtplib
from email.message import EmailMessage
import datetime
import visualization
import updateDB
import manageDB

manageDB.setconnection()

visualization.countActivityReposBestAuthor(12)
visualization.countActivityReposBestWorkshop(12)
visualization.countActivityWorkshopBestAuthor(12)

now = datetime.datetime.now()

ADDRESS_FROM = "stephane.poinsart@utc.fr"

LOGIN_FROM = "spoinsar"
PASSWORD_FROM = os.environ["PWMAIL"]

ADDRESS_TO = "stephane.poinsart@utc.fr"

DOMAIN = "smtps.utc.fr"
PORT = 465
# 465 pour SSL
# 587 pour SMTP

# SMTP ou SMTP_SSL selon l'encryptage de l'utc
# with SMTP(DOMAIN, PORT) as smtp:
with smtplib.SMTP_SSL(DOMAIN, PORT) as smtp:
    smtp.login(LOGIN_FROM, PASSWORD_FROM)

    msg = EmailMessage()
    msg['Subject'] = 'Graph Scenari-stat'
    msg['From'] = ADDRESS_FROM
    msg['To'] = ADDRESS_TO

    endname = str(now.year) + "_" + str(now.month) + "_" + str(now.day) + '.png'
    pngfiles = glob.glob('./*' + endname)

    for file in pngfiles:
        with open(file, 'rb') as fp:
            img_data = fp.read()
        msg.add_attachment(img_data, maintype='image',
                subtype=imghdr.what(None, img_data))

    smtp.send_message(msg)
