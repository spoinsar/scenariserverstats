import requests
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
import json
import updateDB as updatedb
import connect_DB as connectdb
import manageDB as managedb
import datetime, time
import traceback
import sys

import config


def reqretry(s, url, data=None):
    result=None
    for retcount in range(0, 2):
        try:
            time.sleep(0.03)
            if (data is None):
                result = s.get(url, auth=HTTPBasicAuth(config.SCUSER,config.SCPASS))
            else:
                result = s.post(url, data=data, auth=HTTPBasicAuth(config.SCUSER,config.SCPASS))
            if (result is not None):
                if (str(result.content)[:100].find('<message type="Error"')==-1):
                    return result
                else:
                    sys.stderr.write("http request returned error message: "+str(result.content)[:1000]+"\n")
                    time.sleep(0.2)
            else:
                sys.stderr.write("http request returned empty result")
                time.sleep(0.2)
        except Exception as e:
            traceback.print_exc()
            sys.stderr.write("http request failed:"+url+ "\n=== EXCEPTION ===\n")
            time.sleep(0.5)
    sys.stderr.write("http request failed too many times")
    return None;

