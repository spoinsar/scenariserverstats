import pymysql
import datetime, time
import requests
import datetime
from requests.auth import HTTPBasicAuth
import traceback
import sys, json
import connect_DB as connectdb
import manageDB as managedb
import initialize as init
import config

from withretry import reqretry

#def setconnection():
#        global db 
#        db = pymysql.connect(host=config.DBHOST, user=config.DBUSER, password=config.DBPASS, db=config.DBNAME)
#        global cursor 
#        cursor = db.cursor()
    
#domaines=["chim", "math","phys", "bio" ,"geo", "info"]
#niveaux=["L0", "L1", "L2", "L3", "M1", "M2"]
s = requests.Session()
time_now = int(time.mktime(datetime.datetime.now().timetuple()))

authorcache=set();


def checkUpdateTable():
    #ask for timelimit to user or take date of last update by default
    request = reqretry(s, config.SCURL+'/s/~admin/u/adminWsp?cdaction=List&withWspProperties=true&fields=basis*srcRoles*srcRi*srcUser')
    list_workshops = request.content
    root = init.ET.fromstring(list_workshops)
    db_changed = False
    for child in root:        
        derive = False
        new_items = {}
        if(child.get('title')):
            code_workshop = child.get('code')
            title = child.get('title')
            # print(child.get('title'))
            for child_property in child.iter('wspProperties'):
                if(child_property.get('drvAxis')):
                    parent =''
                    derive = True
                    # print('..Atelier derive')
                    parent = child_property.get('drvAxis') # unused but list of parent possible
                break
            if managedb.checkExistenceWorkshop != ():
                timeline = managedb.getLastUpdate() # get date of the last update (or initialization if first update)
                # print(timeline)
                if derive == True:
                    searchstart='<and>'
                    searchend='<exp type="DrvStates" uiCrit="critDrvProps" drvStates="overridenNew overridenDone overridenDirty createdNew createdDone createdDirty"/></and>'
                else:
                    searchstart='""'
                    searchend='""'

                request_check_items={'request':'<request><select max="2001"><column dataKey="srcUri"/><column dataKey="srcId"/><column dataKey="srcSt"/><column dataKey="srcDt"/><column dataKey="srcRi"/><column dataKey="itTi"/><column dataKey="itSt"/><column dataKey="itSgn"/><column dataKey="itPack"/><column dataKey="itModel"/><column dataKey="lcSt"/><column dataKey="srcRoles"/><column dataKey="srcVersLabel"/><column dataKey="srcVersDt"/><column dataKey="srcLiveUri"/><column dataKey="srcUser"/><column dataKey="fullTextScore"/></select><where>'+searchstart+'<exp type="ItemSrcType" uiCrit="filterItems" srcTypes="12"/><exp type="TreeLastModif" uiCrit="critSrcLastModif" op="gt=" value="'+timeline+'"/>'+searchend+'</where></request>'}
                new_items=None
                try:
                    new_items = reqretry(s, config.SCURL+'/s/~admin/u/search?cdaction=Search&param='+code_workshop, data=request_check_items)
                    if (new_items is not None):
                        new_items = json.loads(new_items.content)
                        new_items = new_items['results']
                except Exception as e:
                    sys.stderr.write("erreur de liste des items pour l'atelier: "+code_workshop+", erreur: "+str(e))
                    traceback.print_exc()
                    new_items = None
                if new_items is None or new_items == []:
                    print('nothing new / to update here')
                else:
                    print('NEW ITEMS / ITEMS MODIF')
                    for item in range(0,len(new_items)):
                            code_slice = str(new_items[item][1])
                            new_item = {
                                'path' : str(new_items[item][0]),
                                'code' : code_slice[3:]
                            }
                            new_items[item] = new_item
                    # print(new_items)

                    for index in range(0,len(new_items)): # insert modif / new items / author and history
                        item = new_items[index]
                        if managedb.checkExistenceItem(item['code']) == (): # new item
                            if managedb.checkExistenceWorkshop(code_workshop) == (): # new ws
                                managedb.insertWorkshop(code_workshop,title,derive)
                            managedb.insertItem(item['code'], item['path'],code_workshop)
                            init.historyListItems([item],code_workshop) # use insertion functions of initialize file to insert author and history
                        else: # the item was modified from timeline
                            # if the item, originally in a parent workshop, was modified in a derived workshop : not 
                            new_history = getHistoryFromTimeline(item,timeline, code_workshop) # insert new history
                            # print(new_history)
                            # print('New History insered')
                    db_changed = True
    if db_changed == True:
        managedb.insertLastDateUpdate(time_now)
    return time_now

    



# insert new history of an item from the date of the last update (timeline)
def getHistoryFromTimeline(item,timeline,code_workshop):
    try:
        request_history = reqretry(s, config.SCURL+'/s/~superadmin/u/history?cdaction=ListHistoryNodes&param='+code_workshop+'&refUriLive='+item['path']+'&fields=srcUri*srcId*srcSt*srcDt*srcRi*itTi*itSt*itSgn*itPack*itModel*lcSt*srcRoles*srcVersLabel*srcVersDt*srcLiveUri*srcTreeDt*srcUser*metaFlag*metaComment')
        if (request_history is not None):
            history = json.loads(request_history.content)
            history = history['histNodes']
        else:
            return None
    except Exception as e:
            traceback.print_exc()
            sys.stderr.write("erreur de chargement d'historique pour l'item "+item['path']+", contenu: "+str(request_history.content,'utf-8')+"\n")
            return

    history_item = {}
    lastauthor=None
    lasttimestamp=None
    for hist in range(0,len(history)):
        timestamp = datetime.datetime.fromtimestamp(float(str(history[hist]['srcDt']))/1000) # avoir si on ne garde pas plutot des entiers pour les dates
        author=str(history[hist]['srcUser']);
        history_data = {
            'date' : timestamp,
            'author' : author,
            'item': str(item['code'])
        }
        if (author is not None and author!="" and (lastauthor is None or lasttimestamp is None or ((lasttimestamp-timestamp)<datetime.timedelta(seconds=120) and lastauthor==author))):
            lasttimestamp=timestamp
            lastauthor=author
        init.insertHistoryAndAuthor(history_data)
    return history_item


if __name__ == '__main__':
    managedb.setconnection()
    checkUpdateTable() # check for update in the items : those news or modified
    #print(managedb.getLastUpdate())
    # print(time_now)


