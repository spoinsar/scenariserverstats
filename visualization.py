import pandas as pd
from plotnine import * #useful for ggplot
import plotnine
import platform
import matplotlib.pyplot as plt
import datetime
import manageDB as managedb
import os
import pprint


now = datetime.datetime.now()

# print(platform.python_version())
#ggplot

#preparer les donnees de la db
#rint(histories[0]['date'].strftime('%B'))

def countActivityMonthAuthor(author):
    history = managedb.getHistoryFromAuthor(author)
    activity = [0] * 12

    for modif in history:
        activity[modif['date'].month-1] +=1
        #print modif['date'].isocalendar()[1]

    timeline = {
        'Janvier':{'activity':activity[0]},
        'Fevrier':{'activity':activity[1]},
        'Mars':{'activity':activity[2]},
        'Avril':{'activity':activity[3]},
        'Mai': {'activity':activity[4]},
        'Juin':{'activity':activity[5]},
        'Juillet':{'activity':activity[6]},
        'Aout':{'activity':activity[7]},
        'Septembre':{'activity':activity[8]},
        'Octobre':{'activity':activity[9]},
        'Novembre':{'activity':activity[10]},
        'Decembre':{'activity':activity[11]}
    }

    allActivityAuthor = []
    for key,time in timeline.items():
        activityAuthor = {'user':author, 'month' : key,'activity' : time['activity']}
        allActivityAuthor.append(activityAuthor)
    #print allActivityAuthor
    return  allActivityAuthor

def filterNoContributing(items):
    for element in items:
        if (element.get('count') == 0):
            items.pop(element)

    return items

def filterBestAuthor(authors):
    # Filtrer les contributions des auteurs par atelier
    return authors

def filterBestWorkshop(workshop):
    pass

# Récupère les activités des auteurs au niveau global
def countActivityReposBestAuthor(period=12):
    name = "Meilleurs_Auteurs_Global_" + str(now.year) + "_" + str(now.month) + "_" + str(now.day) + ".png"

    globalActivity = managedb.getAuthorsReposActivity(period)
    #    pp = pprint.PrettyPrinter(indent=4, width=200)
    #    pp.pprint(globalActivity)

    max_activity = globalActivity[0].get('count')

    bestAuthors = filterNoContributing(globalActivity)
                
    if (bestAuthors.__len__() > 0):
        bestAuthorData = pd.DataFrame(data=bestAuthors)

        my_breaks = (1, max_activity//10, max_activity)
        graph = (ggplot(bestAuthorData, aes(x='week', y='author', fill='count'))
                + geom_tile(color="white")
                + scale_fill_gradient(low='#CCFFCC', high='#00A000', name="Activités", trans = "log", breaks = my_breaks)
                + labs(title ='Activite des contribueurs')
                + theme(panel_background=element_rect(fill='white'))
                )
        graph.save(name, format='png')

    # TODO
    # Creer un fichier contenant la liste des auteurs sans activité
    # otherAuthorData = pd.DataFrame(data=authorActivity)
    # print(otherAuthorData)

# Récupère l'activité de chaque atelier
def countActivityReposBestWorkshop(period=12):
    name = "Meilleur_Ateliers_Global_" + str(now.year) + "_" + str(now.month) + "_" + str(now.day) + ".png"

    workshopActivity = managedb.getWorkshopActivity(period)
    max_activity = workshopActivity[0].get('count')

    bestWorkshops = filterNoContributing(workshopActivity)

    if (bestWorkshops.__len__() > 0):
        bestWorkshopData = pd.DataFrame(data=bestWorkshops)

        my_breaks = (1, max_activity//10, max_activity)
        graph = (ggplot(bestWorkshopData, aes(x='week', y='workshop', fill='count'))
                + geom_tile(color="white")
                + scale_fill_gradient(low='#CCFFCC', high='#00A000', name="Activités", trans = "log", breaks = my_breaks)
                + labs(title ='Activite des ateliers')
                + theme(panel_background=element_rect(fill='white'))
                )
        graph.save(name, format='png')

# Récupère les activités des auteurs par atelier
def countActivityWorkshopBestAuthor(period=12):
    workshopActivity = managedb.getAuthorsWorkshopActivity(period)
    workshopSet = set()

    # On créé une liste de tous les ateliers
    for item in workshopActivity:
        workshopSet.add(item.get('workshop'))

    print(workshopSet)

    for workshop in workshopSet:
        name = "Meilleur_Auteurs_De_" + workshop + "_" + str(now.year) + "_" + str(now.month) + "_" + str(now.day) + ".png"
        workshopAuthor = []

        for element in workshopActivity:
            if (element.get('workshop') == workshop):
                workshopAuthor.append(element)
        
        bestWorkshopAuthor = filterNoContributing(workshopAuthor)
        if (bestWorkshopAuthor.__len__() > 0):
            bestWorkshopAuthorData = pd.DataFrame(data=bestWorkshopAuthor)
            my_breaks = (1, 10, 100)
            graph = (ggplot(bestWorkshopAuthorData, aes(x='week', y='author', fill='count'))
                    + geom_tile(color="white")
                    + scale_fill_gradient(low='#CCFFCC', high='#00A000', name="Activités", trans = "log", breaks = my_breaks)
                    + labs(title ='Activite des contributeurs de l\'atelier '+workshop)
                    + theme(panel_background=element_rect(fill='white'))
                    )
            graph.save(name, format='png')

def activityAllAuthors():
    managedb.setconnection()
    authors = managedb.getAuthors()
    allActivity = []
    activity = []
    for author in authors:
        activity = countActivityMonthAuthor(author['name'])
        #print activity
        allActivity.extend(activity)
    return allActivity

# Test des fonctions 
countActivityReposBestAuthor(period=12)
countActivityReposBestWorkshop(period=12)
countActivityWorkshopBestAuthor(period=12)

#parameters
'''
fig = (
    ggplot(data.dropna(subset = ['abv'])) +
    geom_bin2d(
        aes(x = 'abv',
            y = 'ibu')
    ) +
    labs(
        title ='Relationship between alcoholic content (abv) and int. bittering untis (ibu)',
        x = 'abv - The alcoholic content by volume',
        y = 'ibu - International bittering units',
    ) +
    scale_x_continuous(
        limits = (0, 0.14),
        labels = labels(0, 0.14, 0.02),
        breaks = breaks(0, 0.14, 0.02)
    )  +
    scale_y_continuous(
        limits = (0, 150),
        labels = labels(0, 150, 30),
        breaks = breaks(0, 150, 30)
    ) +
    theme(figure_size = (8, 8))
)
'''
