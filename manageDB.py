import connect_DB as connectdb
import pymysql
import datetime
import sys
import config
from operator import itemgetter

def setconnection():
    '''Connexion à la BDD via les variables du fichier config'''
    global db
    global cursor
    db = pymysql.connect(host=config.DBHOST, user=config.DBUSER, password=config.DBPASS, db=config.DBNAME)
    cursor = db.cursor()

def resetAndSetUpDB():
    connectdb.resetDB()

    #connectdb.setupDB()

def checkExistenceWorkshop(ws):
    sql = "SELECT * FROM Workshop WHERE code=%s"
    cursor.execute(sql,(ws))
    return cursor.fetchall()

def checkExistenceAuthor(author):
    sql = "SELECT * FROM Author WHERE name=%s"
    cursor.execute(sql,(author))
    return cursor.fetchall()

def checkExistenceItem(itemCode):
    sql = "SELECT * FROM Item WHERE code=%s"
    cursor.execute(sql,(itemCode))
    return cursor.fetchall()

# We use db.commit to commit pending transaction in the DB
def insertWorkshop(wscode,wstitre,wsderive):
    db = pymysql.connect(host=config.DBHOST, user=config.DBUSER, password=config.DBPASS, db=config.DBNAME)
    cursor = db.cursor()
    sql = "INSERT IGNORE INTO Workshop VALUES(%s, %s,%s)"
    cursor.execute(sql, (wscode, wstitre,wsderive))
    db.commit()

def insertItem(code, path,ws):
    cursor = db.cursor()
    sql = "INSERT IGNORE INTO Item(code, path, workshop) VALUES (%s, %s,%s)"
    # print("insert item " + code + ", " + path + ", " + ws);
    cursor.execute(sql, (code, path,ws))
    db.commit()

def insertHistoryItem(codeItem, date, author):
    sql = "INSERT IGNORE INTO History(date, author,item) VALUES (%s,%s,%s)"
    cursor.execute(sql, (date, author,codeItem))
    db.commit()

def insertAuthor(author):#doesn't matter the warning of duplicate key
    sql = "INSERT IGNORE INTO `Author` VALUES (%s)"
    cursor.execute(sql, (author))
    db.commit()

def getItems():
    sql = "SELECT * FROM Item"
    cursor.execute(sql)
    items = []
    for row in cursor.fetchall():
        line = {
                'codeItem' : row[0],
                'path' : row[1],
                'workshop':row[2]
        }
        items.append(line)

    return items

def getHistoryItem(item):
    sql="SELECT * FROM History WHERE item=%s"
    cursor.execute(sql, (item))
    return cursor.fetchall()

def getAllHistory():
    sql="SELECT * FROM History"
    cursor.execute(sql)
    history = []
    for row in cursor.fetchall():
        line = {
                'historyId' : row[0],
                'date' : row[1],
                'author' : row[2],
                'codeItem' : row[3]
        }
        history.append(line)

    return history

def getHistoryFromAuthor(author):
    sql="SELECT * FROM History WHERE author=%s"
    cursor.execute(sql, (author))
    history = []
    for row in cursor.fetchall():
        line = {
                'historyId' : row[0],
                'date' : row[1],
                'codeItem' : row[3]
        }
        history.append(line)
    return history

def quantifyAllActivityForAuthor(author):
    sql="SELECT author, COUNT(History.date) AS count FROM History WHERE author=%s GROUP BY author"
    cursor.execute(sql, (author))
    return cursor.fetchall()

def quantifyAllActivityAuthors():
    sql="SELECT author, COUNT(History.date) AS count FROM History GROUP BY author"
    cursor.execute(sql)
    return cursor.fetchall()

def getAuthors():
    sql = "SELECT * FROM Author"
    cursor.execute(sql)
    authors = []
    for row in cursor.fetchall():
        line = {
                'name' : row[0],
        }
        authors.append(line)

    return authors

# return a list of dict with the global activity
def getAuthorsReposActivity(period=12):
    setconnection()
    sql = "SELECT Year(H.date) AS year, Month(H.date) AS month, Weekofyear(H.date) AS week, author, Count(*) AS c \
        FROM History AS H \
        INNER JOIN (SELECT author AS authlimit FROM History AS H where  H.date >= CURDATE() - INTERVAL %s MONTH  GROUP BY authlimit ORDER BY count(*) DESC LIMIT 20) as authfilter ON author=authlimit \
        WHERE H.date >= CURDATE() - INTERVAL %s MONTH \
        GROUP BY Year(H.date), Month(H.date), Weekofyear(H.date), author;"
    cursor.execute(sql, (period, period))
    authorsActivity = []
    # row[0] = YEAR # row[1] = MONTH # row[2] = Week # row[3] = Author # row[4] = Count
    for row in cursor.fetchall():
        d = datetime.date(row[0], row[1], 1)
        data = {'author':row[3], 'year':row[0], 'month':d.strftime("%b"), 'week':row[2], 'count':row[4] }
        authorsActivity.append(data)

    return sorted(authorsActivity, key=itemgetter('count'), reverse=True)
# return {'author':str, 'year':int, 'month':str, 'week':int, 'count':int}

# return a list of dict with all the workshop activity during a period
def getAuthorsWorkshopActivity(period=12):
    setconnection()
    sql = "SELECT Year(H.date) AS year, Month(H.date) AS month, Weekofyear(H.date) AS week, author, titre, Count(*) AS c \
            FROM (SELECT id, date, author, item, Workshop.titre FROM History LEFT JOIN Item ON History.item=Item.code LEFT JOIN Workshop ON Item.workshop=Workshop.code) AS H \
            WHERE H.date >= CURDATE() - INTERVAL %s MONTH \
            GROUP BY Year(H.date), Month(H.date), Weekofyear(H.date), author, titre;"
    cursor.execute(sql, period)
    workshopActivity = []
    # row[0] = YEAR # row[1] = MONTH # row[2] = Week number # row[3] = Author # row[4] = WORKSHOP # row[5] = Count
    for row in cursor.fetchall():
        d = datetime.date(row[0], row[1], 1)
        data = {'author':row[3], 'workshop':row[4], 'year':row[0], 'month':d.strftime("%B"), 'week':row[2], 'count':row[5] }
        workshopActivity.append(data)

    return sorted(workshopActivity, key=itemgetter('count'), reverse=True)
# return {'author':, 'workshop': ,'year':, 'month':, 'week':, 'count':}

def getWorkshopActivity(period=12):
    setconnection()
    sql = "SELECT Year(H.date) AS year, Month(H.date) AS month, Weekofyear(H.date) AS week, H.titre, Count(*) AS c \
            FROM (SELECT id, date, item, Workshop.titre, Workshop.code FROM History LEFT JOIN Item ON History.item=Item.code LEFT JOIN Workshop ON Item.workshop=Workshop.code) AS H \
            INNER JOIN (SELECT workshop AS workshoplimit FROM Item LEFT JOIN History ON History.item=Item.code WHERE History.date >= CURDATE() - INTERVAL %s MONTH  GROUP BY workshoplimit ORDER BY count(*) DESC LIMIT 20) as codefilter ON H.code=workshoplimit \
            WHERE H.date >= CURDATE() - INTERVAL %s MONTH \
            GROUP BY Year(H.date), Month(H.date), Weekofyear(H.date), H.titre;"
    cursor.execute(sql, (period, period))
    workshopActivity = []
    # row[0] = YEAR # row[1] = MONTH # row[2] = WEEK # row[3] = WORKSHOP # row[4] = Count
    for row in cursor.fetchall():
        d = datetime.date(row[0], row[1], 1)
        data = {'workshop':row[3], 'year':row[0], 'month':d.strftime("%B"), 'week':row[2], 'count':row[4] }
        workshopActivity.append(data)

    return sorted(workshopActivity, key=itemgetter('count'), reverse=True)
    # return {'workshop': ,'year':, 'month':, 'week':, 'count':}

# return the list of dict of author and their activities during a period
"""
def getPeriodAuthorActivity(period=12):
    '''Retourne un dictionnaire trié selon le nombre de modifications sur la période
    au niveau global'''
    setconnection()

    sql = 'SELECT author, COUNT(H.date) AS count \
        FROM History H \
        WHERE H.date >= CURDATE() - INTERVAL %s MONTH \
        GROUP BY author'
    cursor.execute(sql, period)
    authorsPeriodActivity = []
    for row in cursor.fetchall():
        data = {'author': row[0], 'count': row[1], 'period': period}
        authorsPeriodActivity.append(data)

    return sorted(authorsPeriodActivity, key=itemgetter('count'), reverse=True)
    # return {'author':, 'count':, 'period':}
"""

mocksItems = [{'code' :'codetest1','path':'path1'},
    {'code':'codetest2','path':'path2'},
    {'code':'codetest3','path':'path3'}
    ]
mockAuthor = [{'name':'michel'},
    {'name':'fabriuu'}]
mocksHistories = [{'date':'1539457895634','code':'codetest2'},
    {'date':'1540567954567','code':'codetest3'},
    {'date':'1539765445654','code':'codetest4'}]


def insertMocksItems():
    for item in mocksItems:
            sql = "INSERT IGNORE INTO `Item` (`code`, `path`) VALUES (%s, %s)"
            cursor.execute(sql, (item['code'], item['path']))
    db.commit()


def insertMocksAuthors():
    for item in mockAuthor:
            sql = "INSERT IGNORE INTO `Author` VALUES (%s)"
            cursor.execute(sql, (item['name']))
    db.commit()


def insertHistoryMocks():
    for item in mocksHistories:
            timestamp = datetime.datetime.fromtimestamp(float(item['date'])/1000)
            sql = "INSERT IGNORE INTO History(date, author,item) VALUES (%s,%s,%s)"
            cursor.execute(sql, (timestamp, mockAuthor[1]['name'],item['code']))
    db.commit()


#get the date of the last db update
def getLastUpdate():
    sql="SELECT * FROM UpdateTime ORDER BY date_update DESC LIMIT 1"
    cursor.execute(sql)
    last_update = str(cursor.fetchall())
    last_update = last_update[2:-4]+'000'
    return last_update


# insert date of the update (if change observed)
def insertLastDateUpdate(date):
    sql = "INSERT IGNORE INTO UpdateTime VALUES (%s)"
    cursor.execute(sql, date)
    db.commit()

if __name__ == '__main__':
    #resetAndSetUpDB()
    setconnection()
    #print(getAllHistory())
    #print(getHistoryItem('0048z1CMcIA1hIKCnVTaIX'))
    #getAuthors()
    #print(getItems())
    #quantifyAllActivityAuthors()
    pass
