import requests
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
import json
import updateDB as updatedb
import connect_DB as connectdb
import manageDB as managedb
import datetime, time
import traceback
import sys

import config
from withretry import reqretry

#domaines=["chim", "math","phys", "bio" ,"geo", "info"]
#niveaux=["L0", "L1", "L2", "L3", "M1", "M2"]
s = requests.Session()


authorcache=set();

#public/u/loginUiMoz
auth = s.get(config.SCURL+'/public/u/loginUiMoz', auth=HTTPBasicAuth(config.SCUSER, config.SCPASS))
time_now = int(time.mktime(datetime.datetime.now().timetuple()))

def historyListItems(list_items, code_workshop):
    # print('CALL')
    # print(list_items)
    for item in range(0,len(list_items)):
        # print('HISTORY :', list_items[item]) ### d
        item_i = list_items[item]
        try:
            request_history = reqretry(s, config.SCURL+'/s/~superadmin/u/history?cdaction=ListHistoryNodes&param='+code_workshop+'&refUriLive='+item_i['path']+'&fields=srcUri*srcId*srcSt*srcDt*srcRi*itTi*itSt*itSgn*itPack*itModel*lcSt*srcRoles*srcVersLabel*srcVersDt*srcLiveUri*srcTreeDt*srcUser*metaFlag*metaComment')
            # print(request_history.content)
            if (request_history is None):
                continue;
            history = json.loads(request_history.content)
            history = history['histNodes']
        except Exception as e:
            traceback.print_exc()
            sys.stderr.write("erreur de chargement d'historique pour l'item "+item_i['path']+", contenu: "+str(request_history.content,'utf-8')+"\n")
            history = None
        if (history is not None):
            lastauthor=None
            lasttimestamp=None
            for hist in range(0,len(history)):
                timestamp = datetime.datetime.fromtimestamp(float(str(history[hist]['srcDt']))/1000) # avoir si on ne garde pas plutot des entiers pour les dates
                author=str(history[hist]['srcUser']);
                history_data = {
                    'date' : timestamp,
                    'author' : author,
                    'item': str(item_i['code'])
                }
                if (author is not None and author!="" and (lastauthor is None or lasttimestamp is None or ((lasttimestamp-timestamp)<datetime.timedelta(seconds=120) and lastauthor==author))):
                    lasttimestamp=timestamp
                    lastauthor=author
                insertHistoryAndAuthor(history_data)
        sys.stdout.write('.')
        # sys.stdout.flush()
    return list_items





def insertHistoryAndAuthor(h):
    if (h['author'] not in authorcache):
        managedb.insertAuthor(h['author'])
        authorcache.add(h['author'])
    #insert history
    managedb.insertHistoryItem(h['item'], h['date'],h['author'])

def insertListItems(list_items,code_workshop):
    # print('NBR ITEMS TO INSERT :'+ str(len(list_items)))
    for item in range(0,len(list_items)):
        # print(list_items[item]['code']) ### d
        managedb.insertItem(list_items[item]['code'], list_items[item]['path'],code_workshop)


def firstInitialize():
    request = reqretry(s, config.SCURL+'/s/~admin/u/adminWsp?cdaction=List&withWspProperties=true&fields=basis*srcRoles*srcRi*srcUser')
    list_workshops = request.content
    #manage xml
    root = ET.fromstring(list_workshops)
    #initialize db's tables
    connectdb.setupDB()
    managedb.setconnection()
    #look in workshop
    for child in root:       
        derive = False
        list_items = {}
        if(child.get('title')):
            code_workshop = child.get('code')
            title = child.get('title')
            # print(child.get('title')) ### d
            for child_property in child.iter('wspProperties'):
                if(child_property.get('drvAxis')):
                    parent =''
                    derive = True
                    #print( '..Atelier derive')
                    parent = child_property.get('drvAxis') # to do : create parents list
                break
            #updatedb.checkUpdateTable(code_workshop,derive)
            managedb.insertWorkshop(code_workshop,title,derive) #insert workshop
            #time.sleep(0.1)- datetime.datetime(2007,1,1))
            timeline = str(int(datetime.datetime.now().strftime("%s")))
            if derive == True:
                searchstart='<and>'
                searchend='<exp type="DrvStates" uiCrit="critDrvProps" drvStates="overridenNew overridenDone overridenDirty createdNew createdDone createdDirty"/></and>'
            else:
                searchstart='""'
                searchend='""'

            request_check_items={'request':'<request><select max="15000"><column dataKey="srcUri"/><column dataKey="srcId"/><column dataKey="srcSt"/><column dataKey="srcDt"/><column dataKey="srcRi"/><column dataKey="itTi"/><column dataKey="itSt"/><column dataKey="itSgn"/><column dataKey="itPack"/><column dataKey="itModel"/><column dataKey="lcSt"/><column dataKey="srcRoles"/><column dataKey="srcVersLabel"/><column dataKey="srcVersDt"/><column dataKey="srcLiveUri"/><column dataKey="srcUser"/><column dataKey="fullTextScore"/></select><where>'+searchstart+'<exp type="ItemSrcType" uiCrit="filterItems" srcTypes="12"/><exp type="TreeLastModif" uiCrit="critSrcLastModif" op="gt=" value="'+timeline+'"/>'+searchend+'</where></request>'}

            try:
                content = reqretry(s, config.SCURL+'/s/~admin/u/search?cdaction=Search&param='+code_workshop, data=request_check_items)
                if (content is not None):
                    content = json.loads(content.content)
                    content = content['results']
            except Exception as e:
                sys.stderr.write("erreur de listing pour l'atelier "+title+", erreur: "+str(e))
                traceback.print_exc()
                content=None
            #request_items={'request':'<request><select max="8001"><column dataKey="srcUri"/><column dataKey="srcId"/><column dataKey="srcSt"/><column dataKey="srcDt"/><column dataKey="srcRi"/><column dataKey="itTi"/><column dataKey="itSt"/><column dataKey="itSgn"/><column dataKey="itPack"/><column dataKey="itModel"/><column dataKey="lcSt"/><column dataKey="srcRoles"/><column dataKey="srcVersLabel"/><column dataKey="srcVersDt"/><column dataKey="srcLiveUri"/><column dataKey="srcUser"/><column dataKey="fullTextScore"/></select><where><exp type="ItemSrcType" uiCrit="filterItems" srcTypes="12"/><and><exp type="ItemSgnRegexp" uiCrit="critItemType" regexpSgn="(@op_mcqSur|@op_mcqMur)\\b.*" uiTypes="op_mcqSur|op_mcqMur"/><not uiOp="or"><exp type="LifeCycleStates" uiCrit="critLifeCycleProps" lcStates="reject"/></not></and></where></request>'}
            #items = s.post(url+'/s/~admin/u/search?cdaction=Search&param='+code_workshop, data=request_items, auth=HTTPBasicAuth(login, password))
            if (content is not None):
                for item in range(0,len(content)):
                    code_slice = str(content[item][1])
                    new_item = {
                        'path' : str(content[item][0]),
                        'code' : code_slice[3:]
                    }
                    list_items[item] = new_item
                # print('REQUEST')
                # print(list_items)
                insertListItems(list_items,code_workshop)
                historyListItems(list_items,code_workshop)
                #history_list = historyListItems(list_items,code_workshop)
    managedb.insertLastDateUpdate(time_now)



if __name__ == '__main__': # only execute in this file, not by the import in other file
    firstInitialize() # to do once

